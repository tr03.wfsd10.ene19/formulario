var preguntas = new Array(

    {
        foto:"img/4.png",
        titulo: "Pregunta 1",
        pregunta:"Las deducciones del trabajador se calculan:",
        respuesta1:"Establecidas por acuerdo entre empresario y empleado.",
        respuesta2:"A partir de la base de cotización",
        respuesta3:"Son establecidas por el empresario.",
        solucion: "2",
    },
    {
        foto:"img/5.png",
        titulo: "Pregunta 2",
        pregunta:"¿Puede un trabajador, renunciar a los días de descanso semanales que le pertenecen según su tipo de contrato?",
        respuesta1:"Si, es lo correcto para ayudar al buen funcionamiento de la empresa.",
        respuesta2:"No, no se puede renunciar a un derecho del trabajador",
        respuesta3:"A veces, es necesario y se hacen excepciones.",
        solucion: "2",
    },
    {
        foto:"img/1.png",
        titulo: "Pregunta 3",
        pregunta:"¿Cuál de estas causas no supone la extinción del contrato?",
        respuesta1:"ERE",
        respuesta2:"Abandono",
        respuesta3:"Maternidad",
        solucion: "3",
    },
    {
        foto:"img/2.png",
        titulo: "Pregunta 4",
        pregunta:"En un contrato de trabajo en grupo, la responsabilidad recae:",
        respuesta1:"Recae en el representante del grupo",
        respuesta2:"Se reparte a partes iguales entre todos los componentes",
        respuesta3:"Por igual entre cada uno de los componentes",
        solucion: "1",
    },
    {
        foto:"img/3.png",
        titulo: "Pregunta 5",
        pregunta:"¿Dónde se regula la huelga en la Constitución?",
        respuesta1:"Artículo 27",
        respuesta2:"Artículo 29",
        respuesta3:"Artículo 28",
        solucion: "3",
    },
);


